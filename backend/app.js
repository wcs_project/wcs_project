const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Argonaute = require('./models/Argonaute');

mongoose.connect('mongodb+srv://mathilde_v:c"kP(Vl>@projectswcs.p7mda.mongodb.net/argonautes?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

const app = express();

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

app.use(bodyParser.json());

app.post('/api/argonaute', (req, res, next) => {
    delete req.body._id;
    
    const argonaute = new Argonaute({
        ...req.body
    });

    argonaute.save()
    .then(() => res.status(201).json({ message: 'Argonaute enregistré!'}))
    .catch(error => res.status(400).json({ error }))
})

app.delete('/api/argonaute/:id', (req, res, next) => {
  Argonaute.deleteOne({ _id: req.params.id })
    .then(() => res.status(200).json({ message: 'Argonaute supprimé !'}))
    .catch(error => res.status(400).json({ error }));
});

app.use('/api/argonaute', (req, res, next) => {
    Argonaute.find()
    .then(argonautes => res.status(200).json(argonautes))
    .catch()
})
module.exports = app;