import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { MaterialModule } from './shared/module/material/material.module';

const routes: Routes = [
  {
    path: 'argonaute',
    loadChildren: () => import('./page/argonaute/argonaute.module').then((m) => m.ArgonauteModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), MaterialModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
