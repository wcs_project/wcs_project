import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ArgonauteFormComponent, ArgonautesListComponent } from './components/containers';
import { MaterialModule } from 'src/app/shared/module/material/material.module';
import { ConfirmDialogComponent } from './components/modals';
import { ArgonautesPage } from './page/argonautes.page';

const CONTAINERS = [ArgonauteFormComponent, ArgonautesListComponent];
const PAGE = [ArgonautesPage];
const MODAL = [ConfirmDialogComponent];

const route: Routes = [{ path: '', pathMatch: 'full', component: ArgonautesPage }];

@NgModule({
  declarations: [CONTAINERS, PAGE, MODAL],
  imports: [CommonModule, RouterModule.forChild(route), FormsModule, MaterialModule, ReactiveFormsModule],
})
export class ArgonauteModule {}
