import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArgonautesPage } from './argonautes.page';

describe('ArgonautesPage', () => {
  let component: ArgonautesPage;
  let fixture: ComponentFixture<ArgonautesPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ArgonautesPage],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArgonautesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
