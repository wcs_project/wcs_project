import { Component, OnInit } from '@angular/core';

import { ArgonauteService } from 'src/app/shared/service/argonaute.service';
import { Argonaute } from 'src/app/shared/interface/argonaute.interface';

@Component({
  templateUrl: './argonautes.page.html',
  styleUrls: ['./argonautes.page.scss'],
})
export class ArgonautesPage implements OnInit {
  argonautes: Array<Argonaute>;

  constructor(private argonauteSvc: ArgonauteService) {}

  public ngOnInit(): void {
    this.getArgonautes();
  }

  public getArgonautes(): void {
    this.argonauteSvc.getArgonautes().subscribe((data) => {
      this.argonautes = data;
    });
  }
}
