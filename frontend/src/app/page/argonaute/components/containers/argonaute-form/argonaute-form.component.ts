import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ArgonauteService } from 'src/app/shared/service/argonaute.service';
import { Argonaute } from 'src/app/shared/interface/argonaute.interface';

@Component({
  selector: 'app-argonaute-form',
  templateUrl: './argonaute-form.component.html',
  styleUrls: ['./argonaute-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArgonauteFormComponent implements OnInit {
  @Output() private reloadEvent: EventEmitter<any> = new EventEmitter();

  public argonauteForm: FormGroup;
  public newArgonaute: Argonaute;

  constructor(private formBuilder: FormBuilder, private argonauteSvc: ArgonauteService) {}

  public ngOnInit(): void {
    this.argonauteForm = this.formBuilder.group({
      name: [null, Validators.required],
    });
  }

  public addArgonaute(): void {
    this.newArgonaute = this.argonauteForm.value;

    this.argonauteSvc.createArgonaute(this.newArgonaute).subscribe(() => this.reloadEvent.emit());
  }
}
