import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArgonautesListComponent } from './argonautes-list.component';

describe('ArgonautesListComponent', () => {
  let component: ArgonautesListComponent;
  let fixture: ComponentFixture<ArgonautesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArgonautesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArgonautesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
