import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { ConfirmDialogComponent } from 'src/app/page/argonaute/components/modals';
import { ArgonauteService } from 'src/app/shared/service/argonaute.service';
import { Argonaute } from 'src/app/shared/interface/argonaute.interface';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-argonautes-list',
  templateUrl: './argonautes-list.component.html',
  styleUrls: ['./argonautes-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArgonautesListComponent {
  @Output() private reloadEvent: EventEmitter<any> = new EventEmitter();

  @Input() public argonautes: Array<Argonaute>;

  constructor(private argonauteSvc: ArgonauteService, private dialog: MatDialog) {}

  public deleteArgonaute(value: Argonaute): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);

    dialogRef.afterClosed().subscribe((confirm) => {
      if (!confirm) {
        return;
      }

      this.argonauteSvc.deleteArgonaute(value._id).subscribe(() => this.reloadEvent.emit());
    });
  }
}
