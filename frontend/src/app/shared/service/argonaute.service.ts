import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Argonaute } from 'src/app/shared/interface/argonaute.interface';

@Injectable({
  providedIn: 'root',
})
export class ArgonauteService {
  constructor(private httpClient: HttpClient) {}

  public getArgonautes(): Observable<Array<Argonaute>> {
    return this.httpClient.get<Array<Argonaute>>('http://localhost:3000/api/argonaute');
  }

  public createArgonaute(newArgonaute: Argonaute): Observable<Argonaute> {
    return this.httpClient.post<Argonaute>('http://localhost:3000/api/argonaute', newArgonaute);
  }

  public deleteArgonaute(id: number) {
    return this.httpClient.delete<Argonaute>('http://localhost:3000/api/argonaute/' + id);
  }
}
