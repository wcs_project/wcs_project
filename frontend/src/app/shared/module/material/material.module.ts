import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [MatIconModule, MatInputModule, MatFormFieldModule, MatButtonModule, MatDialogModule],
  exports: [MatIconModule, MatInputModule, MatFormFieldModule, MatButtonModule, MatDialogModule],
})
export class MaterialModule {}
