import { NumberValueAccessor } from '@angular/forms';

export interface Argonaute {
  _id: number;
  name: string;
}
